import Link from "next/link";
import React from "react";
import Image from "next/image";

function Logo() {
  return (
      <Link
        href="/"
        title="VetEnfant"
        className="relative w-28 h-full "
      >

        <Image
          src={`/Logos/LogoVet.png`}
          fill
          alt={"Logo VetEnfant"}
          className="object-cover" />
    
      </Link>
    
  );
}

export default Logo;
