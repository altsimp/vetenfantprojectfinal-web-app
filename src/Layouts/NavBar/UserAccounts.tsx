import React, { useRef, useState } from "react";
import { FaAngleDown } from "react-icons/fa";
import { UserIcon } from "@heroicons/react/24/outline";
import Link from "next/link";

export default function UserAccounts() {
  const [open, setOpen] = useState(false);
  const dropdownRef = useRef<HTMLDivElement>(null);
  const handleDropDownFocus = (state: boolean) => {
    setOpen(!state);
  };

  console.log(open);

  return (
    <div className="">
      <div className="app drop-down-container" ref={dropdownRef}>
        <button
          onClick={(e) => handleDropDownFocus(open)}
          className="hidden lg:flex"
        >
          <UserIcon className="icon-button" />
          {/* <span>Me connecter</span> */}
          <FaAngleDown className="mt-1 ml-0.5 text-zinc-950 " />
        </button>
        {open && (
          <ul>
            <li className="">
              <h2>Déja client</h2>
              <Link href={"/login"}>
                <button className="lg:flex py-2 px-2 mr-4 bg-white border-2 border-black font-normal rounded-md shadow-md hover:bg-[--yellowBalke] focus:outline-none focus:ring focus:ring-black focus:ring-opacity-75">
                  ME CONNECTER
                </button>
              </Link>
            </li>
            <li className="">
              <Link href={"/forgot-password"}>
                <h2 className=" font-light text-slate-200">
                  Mot de passe oublié ?
                </h2>
              </Link>
            </li>
            <hr className="mt-4 mb-4" />
            <li className="">
              <h2 className="">Nouveau client ?</h2>
              <Link href={"/register"}>
                <button className="lg:flex py-2 px-2 mr-4 bg-white border-2 border-black font-normal rounded-md shadow-md hover:bg-[--yellowBalke] focus:outline-none focus:ring focus:ring-black focus:ring-opacity-75">
                  S'INSCRIRE
                </button>
              </Link>
            </li>
            <hr className="mt-4 mb-4" />
            {/* Quand l'utilisateur est connecter */}
            <li className="">
              <Link href={"/"}>
                <h2 className=" font-light text-slate-200">Me deconnecter</h2>
              </Link>
            </li>
          </ul>
        )}
      </div>
    </div>
  );
}
