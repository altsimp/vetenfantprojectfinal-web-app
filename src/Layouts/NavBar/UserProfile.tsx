import UserDropDown from "@/src/components/UserDropDown";
import React from "react";

function UserProfile() {
  return (
    <div className="hidden lg:flex p-40">
      <UserDropDown
        onOptionClick={(option) => {
          console.log(option)
        }}
    
      />
    </div>
  );
}

export default UserProfile;
