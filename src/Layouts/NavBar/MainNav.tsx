
import React from "react";
import Search from "./Search";
import UserAccounts from "./UserAccounts";
import UserProfile from "./UserProfile";


export default function MainNavbar() {
  return (
    /* bouton vendre des articles */
    <div className="flex items-center w-75 gap-3 xl:justify-end lg:justify-around ">
      
       <Search />

      <UserProfile/>
      
    </div>
  );
}
