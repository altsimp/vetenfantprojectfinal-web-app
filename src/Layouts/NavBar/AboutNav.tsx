import Link from "next/link";
import React from "react";

export default function AboutNav() {
  return (
    <div className="max-w-container mx-auto lg:flex mt-4 mb-4 px-4">
      <ul className="hidden lg:flex space-x-20 font-poppins">
        <li>
          <Link href={"/products"} className="mr-4">
            <h2 className="link-title">{`NOUVEAUTÉS`}</h2>
            <span className="link-subtitle">{`0-16 ans`}</span>
          </Link>
        </li>
        <li className="ml-4">
          <Link href={"/product/baby"} className="mr-4">
            <h2 className="link-title">{`BÉBÉ`}</h2>
            <span className="link-subtitle">{`0-3 ans`}</span>
          </Link>
        </li>
        <li>
          <Link href={"/products/girl"} className="mr-4">
            <h2 className="link-title">{`FILLE`}</h2>
            <span className="link-subtitle">{`3-16 ans`}</span>
          </Link>
        </li>
        <li>
          <Link href={"/products/boy"} className="mr-4">
            <h2 className="link-title">{`GARÇON`}</h2>
            <span className="link-subtitle">{`3-16 ans`}</span>
          </Link>
        </li>
      </ul>
      
    </div>
  );
}
