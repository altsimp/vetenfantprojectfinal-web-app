import React from "react";
import FooterSecondarySitemap from "./FooterSecondarySitemap";
/* import FooterMainSitemap from "./FooterMainSitemap"; */

function FooterSitemap() {
  return (
    <div className="flex space-x-10 w-[65%] justify-around">
      {/* <FooterMainSitemap /> */}
      <FooterSecondarySitemap
        title={`CONTACT & AIDE`}
        titleLink={``}
        urlList={[
          {
            title: "Livraison et retour",
            url: "/",
          },
          {
            title: "Nous écrire",
            url: "/",
          },
          {
            title: "FAQ",
            url: "/",
          },
          {
            title: "Guide des tailles",
            url: "/",
          }
        ]}
      />

      <FooterSecondarySitemap
        title={`ÇA C'EST VETENFANT`}
        titleLink={``}
        urlList={[
          {
            title: "A propos de VetEnfant",
            url: "/",
          },
          {
            title: "",
            url: "/",
          },
        ]}
      />
      <FooterSecondarySitemap
        title={`INFORMATION`}
        titleLink={``}
        urlList={[
          {
            title: "Données personnelles",
            url: "/",
          },
          {
            title: "Conditons générales de vente",
            url: "/",
          },
          {
            title: "Mentions légales",
            url: "/",
          }
        ]}
      />
    </div>
  );
}

export default FooterSitemap;
