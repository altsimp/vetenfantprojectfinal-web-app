import Link from "next/link";
import React from "react";
import { FaFacebook, FaInstagram, FaTiktok, FaTwitter, FaYoutube } from "react-icons/fa";

function FooterSocialLinks() {
  return (
    <div className="flex flex-col">
      <span className=" px-10 break-words font-bold uppercase">
        Suivez-nous sur nos réseaux sociaux.
      </span>

      <ul className="flex items-center gap-3 px-10 mt-4 mb-4">
        <li
          className="flex w-8 h-8 rounded-full flex-shrink-0
      transform ease-in-out duration-300
      "
        >
          <Link href={`/`} className="w-full">
            <FaFacebook className="icon-button" />
          </Link>
        </li>

        <li
          className="flex w-8 h-8 rounded-full flex-shrink-0
      transform ease-in-out duration-300
      "
        >
          <Link href={`/`} className="w-full">
            <FaInstagram className="icon-button" />
          </Link>
        </li>

        <li
          className="flex w-8 h-8 rounded-full flex-shrink-0
      transform ease-in-out duration-300
      "
        >
          <Link href={`/`} className="w-full">
            <FaTiktok className="icon-button" />
          </Link>
        </li>
        <li
          className="flex w-8 h-8 rounded-full flex-shrink-0
      transform ease-in-out duration-300
      "
        >
          <Link href={`/`} className="w-full">
            <FaTwitter className="icon-button" />
          </Link>
        </li>
        <li
          className="flex w-8 h-8 rounded-full flex-shrink-0
      transform ease-in-out duration-300
      "
        >
          <Link href={`/`} className="w-full">
            <FaYoutube className="icon-button" />
          </Link>
        </li>
      </ul>

      <span className=" px-10 break-words">
        Promis, ici, les enfants ressemblent aux vôtres.
      </span>
    </div>
  );
}

export default FooterSocialLinks;
