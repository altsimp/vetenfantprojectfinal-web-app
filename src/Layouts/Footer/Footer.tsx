import React from 'react'
import FooterSitemap from './FooterSitemap'
import FooterSocialLinks from './FooterSocialLinks'

function Footer() {
  return (
    <footer
    className='flex flex-col gap-8 p-12 bg-red-300'
    >
      <div
      className='flex w-full justify-between items-start'
      >
         <FooterSocialLinks/>
         
         <FooterSitemap/>

      </div>
     
    </footer>
  )
}

export default Footer
