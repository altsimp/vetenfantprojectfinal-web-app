import React from "react";
import Logo from "./Logo";
import MainNav from "./NavBar/MainNav";
import NavDrawer from "./NavBar/NavDrawer";
import AboutNav from "./NavBar/AboutNav";
import ButtonSellProduct from "../components/button/ButtonSellProduct";

export default function Header() {
  return (
    <div className="w-full h-20 bg-white fixed top-0 z-50 border-b-[1px] border-b-gray-200">
      <nav className="w-full flex justify-around items-center px-10 p-10 lg:px-12 py-4 h-20">
        
        <Logo />

        <AboutNav />
        
        <ButtonSellProduct />
        
        <MainNav />
        


        <NavDrawer />
        
      </nav>
    </div>
  );
}
