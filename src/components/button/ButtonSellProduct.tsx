import Link from "next/link";
import React from "react";

export default function ButtonSellProduct() {
  return (
    <div className="">
      <Link href="/sellProduct">
        <button className="hidden lg:flex py-2 px-2 mr-4 bg-white border-2 border-black font-normal rounded-lg shadow-md hover:bg-[--yellowBalke] focus:outline-none focus:ring focus:ring-black focus:ring-opacity-75">
          Vend tes articles
        </button>
      </Link>
    </div>
  );
}
