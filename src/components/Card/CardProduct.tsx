import { Product } from "@/src/General/Entity";
import Link from "next/link";
import React from "react";

export default function CardProduct({ products }: any) {
  return (
    <div>
      <ul className="flex space-x-4 overflow-x-auto">
        {products && products.length > 0 ? (
          products?.map((product: Product) => (
            <li className="w-96">
              <Link
                href={`/products/${product?.id}/${product?.name}`}
                className="flex flex-col"
              >
                <div className="relative w-full h-[430px] bg-black/10">
                 
                 {/* Image */}
             
               
                 {product?.images && product?.images.length > 0 ? (
                      <img
                        src={product?.images[0].src}
                        alt={product?.images[0].alt}
                        className="w-full h-full object-cover"
                      />
                    ) : (
                    <span className="absolute inset-0 flex items-center justify-center text-gray-500">
                      Aucune image
                    </span>
                  )}
                </div>

                <div className="flex flex-col py-3 space-y-1">
                  <h3 className="font-medium ">{product?.name}</h3>

                  <p className="opacity-80 line-clamp-1">{product?.description}</p>

                  <p className="font-medium pt-1">
                    {product?.price?.toFixed(2)} {"€"}
                  </p>
                </div>
              </Link>
            </li>
          ))
        ) : (
          <span>aucun produit</span>
        )}
      </ul>
    </div>
  );
}
