import React, { FC, useState } from "react";
import { UserIcon } from "@heroicons/react/24/outline";
import { CaretDown, CaretUp } from "phosphor-react";
import Link from "next/link";

const UpTriangle = ({ size }: { size: number }) => {
  const borderStyle = "1px solid rgb(209, 213, 219)";
  return (
    <div
      style={{
        position: `absolute`,
        top: `-4px`,
        left: `10px`,
        width: `${size}px`,
        height: `${size}px`,
        transform: `rotate(45deg)`,
        backgroundColor: `white`,
        borderLeft: borderStyle,
        borderTop: borderStyle,
      }}
    ></div>
  );
};

const UserDropDown: FC<{
  onOptionClick: (option: string) => void;
}> = ({ onOptionClick }) => {
  const [isExpanded, setIsExpanded] = useState(false);
  const showFaAngleDown = isExpanded;
  const showFaAngleUp = !isExpanded;

  return (
    <div className="relative">
      <button
        onClick={() => setIsExpanded(!isExpanded)}
        className="flex items-end"
      >
        <UserIcon className="icon-button mr-1" />

        <div>
          {showFaAngleDown && <CaretDown className="size={16}" />}
          {showFaAngleUp && <CaretUp className="size={16}" />}
        </div>
      </button>

      {isExpanded && (
       
       /* Si l'utilisateur est déconnecter */

        <div className="absolute bg-white rounded border py-4 mt-2 w-60">
          <UpTriangle size={7} />
          <ul className="px-4">
            <li className="">
              <h2 className="font-semibold mb-2">Déjà client ?</h2>
              <Link href={"/login"}>
                <button className="btnCenterContent">ME CONNECTER</button>
              </Link>
            </li>
            <li className="">
              <Link href={"/forgot-password"}>
                <h2 className=" font-light text-sm mt-1 text-[#8e8e8e]">
                  Mot de passe oublié ?
                </h2>
              </Link>
            </li>
            <hr className="mt-4 mb-4 " />
            <li className="">
              <h2 className="font-semibold mb-2 px-2">Nouveau client ?</h2>
              <Link href={"/register"} className="">
                <button className="btnCenterContent">S'INSCRIRE</button>
              </Link>
            </li>
          </ul>
        </div>

        /* Si l'utilisateur est connecter affichera sa photo */

        /* Afficher la photo  à la place de l'icone */

        /* Afficher Mon compte, favoris, Se deconnecter renvoyer vers la page d'accueil */
        
        


        
      )}
    </div>
  );
};

export default UserDropDown;
