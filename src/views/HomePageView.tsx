import React, { useEffect, useState } from 'react'
import Layout from '../Layouts/Layout'
import HomePageBanner from '../HomePage/HomePageBanner'
import HomePageNewProductSection from '../HomePage/HomePageNewProductSection'
import { productService } from '../services/productService';

export default function HomePageView() {

  const [products, setProducts] = useState<any>(null);
  try {
    const getProducts = async () => {
      setProducts(await productService.fetchAll());
    };
    useEffect(() => {
      getProducts();
    }, []);
  } catch (error) {
    console.error("Error fetching product:", error);
    throw error;
  }
  console.log(products);
  
  return (
    <Layout>
      <HomePageBanner/>
      <HomePageNewProductSection products={products}/> 
    </Layout>
  )
}