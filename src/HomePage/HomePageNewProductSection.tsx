import Link from "next/link";
import React from "react";
import HomePageNewProductsList from "./HomePageNewProductList";

function HomePageNewProductSection({ products }: any) {
  return (
    <section className="w-full space-y-10 items-start px-12 py-12 overflow-hidden">
      <div className="w-full flex items-center justify-between flex-col">
        <h2 className="text-2xl font-medium item-center px-5 py-4">{`ÇA VIENT D'ARRIVER`}</h2>

        <h3 className="font-medium item-center">
          {
            "Ici, vous composez les vestiaires de vos kids aussi vite qu’ils grandissent !"
          }
        </h3>
      </div>

      {/* Afficher les produits nouveaux limiter  */}

      <HomePageNewProductsList products={products} />

      <div className="flex justify-center items-center">
        <Link href={"/allProduct"}>
          <button className="py-2 px-2 mr-2 mt-2 w-60 bg-white border-2 border-black font-normal rounded-lg shadow-md hover:bg-[--yellowBalke] focus:outline-none focus:ring focus:ring-black focus:ring-opacity-75">
            Voir plus
          </button>
        </Link>
      </div>
    </section>
  );
}

export default HomePageNewProductSection;
