import React from "react";
import Image from "next/image";
import Link from "next/link";

function HomePageBanner() {
  return (
    <section className="w-full h-[700px] relative overflow-hidden">
      <Image
        src={`/Images/rain-banner.jpg`}
        alt={`photo de publicité`}
        fill
        className="object-cover"
      />

      <div
        className="absolute top-0 left-0 w-full h-full p-10 
    flex flex-col justify-end space-y-10"
      >
        <div className=" size-60 text-black">
          <h2 className="font-Sue_Ellen_Francisco text-xl font-bold">
            {`Rendez la garde-robe de votre enfant plus lumineuse avec nous`}
          </h2>
        </div>
      </div>
    </section>
  );
}
export default HomePageBanner;
