import React, { useEffect, useState } from "react";
import { Product } from "../General/Entity";
import Link from "next/link";
import { imageService } from "../services/imageService";
import CardProduct from "../components/Card/CardProduct";

export default function HomePageNewProductsList({ products, img }: any) {
  
/* const [images, setImages] = useState<any>(null);
  try {
    const getImages = async () => {
      setImages(await imageService.fetchAll());
    };
    useEffect(() => {
      getImages();
    }, []);
  } catch (error) {
    console.error("Error fetching image:", error);
    throw error;
  }
  console.log(images); */

  return (

    <CardProduct products={products}/>

  );
}
