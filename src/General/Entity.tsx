export interface User {
    id?:number;
    accountName?:string;
    email?:string;
    password?:string;
    role?:string;
    name?:string;
    firstName?:string;
    phoneNumber?:string;
    description?:string;
    profileImage?:string;
}

export interface Product{
    length: number;
    id?:number;
    name?:string;
    gender?:string;
    size?:string;
    price?:number;
    description?:string;
    seller?:User;
    isSold?:boolean;
    category?:string;
    season?:string;
    state?:string;
    images?:Image[];
}


export interface Image{
    id?:number;
    src?:string;
    title?:string;
    alt?:string;
}

export interface Category{
    id?:number;
    type?:string;
    products?:Product[];
}

export interface Message{
    id?:number;
    sender?:User;
    receiver?:User;
    content?:string;
    product?:Product;
    date?:Date;
}

export interface Adress{
    id?:number;
    userId?:number;
    postalCode?:string;
    city?:string;
    street?:string;
}
