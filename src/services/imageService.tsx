import axios from "axios";
import { Image } from "../General/Entity";

interface Props {
    image : Image;
}

export const imageService = {
    async fetchAll(){
        const response = await axios.get(`${process.env.NEXT_PUBLIC_SERVER_URL}/api/image`);
        return response.data;
    }
}