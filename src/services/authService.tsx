import axios from "axios";
import { parseCookies } from "nookies";
import { User } from "../General/Entity";

export const signin = async (user:User)=>{
    const response = await axios.post(`${process.env.NEXT_PUBLIC_SERVER_URL}/api/register`,user);
    return response;
}

export async function login(email:string, password:string){
    const response = await axios.post(`${process.env.NEXT_PUBLIC_SERVER_URL}/api/login`,{email,password});
    console.log(response)
    return response.data.token;
}

export async function fetchUser(){
    const { token } = parseCookies();
    const response = await axios.get<User>(`${process.env.NEXT_PUBLIC_SERVER_URL}/api/account` ,{
        headers: {
            "Authorization" : `Bearer ${token}`,
            'Content-Type': 'application/json'
        }
    });
    return response.data;
}
