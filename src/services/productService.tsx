import axios from "axios"
import { useEffect, useState } from "react";
import { Product } from "../General/Entity";

/* Création d'une interface Props avec mes produits nommée dans Entity*/
interface Props {
    product: Product;
}

/* Création d'un produit service qui me permettra de recuperer les informations du back et de les afficher dans ma vue */
export const productService = {
    async fetchAll(){
        const response = await axios.get(`${process.env.NEXT_PUBLIC_SERVER_URL}/api/product`);
        return response.data;
    }
}

